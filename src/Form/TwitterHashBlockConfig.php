<?php

namespace Drupal\twitter_search_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides configuration options for TWitter API.
 */
class TwitterHashBlockConfig extends ConfigFormBase {

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'block_config_form_id';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['twitter_search_block.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config_tweet = $this->configFactory->get('twitter_search_block.settings');
    $form['tweets_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of Tweets'),
      '#required' => TRUE,
      '#default_value' => $config_tweet->get('tweets_count'),
      '#description' => $this->t('Number of embedded tweets thet gets displayed inside the Block.'),
    ];
    $form['search_query_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter Search Value'),
      '#required' => FALSE,
      '#default_value' => $config_tweet->get('search_query_value'),
      "#description" => $this->t('This search value is used to display the embedded tweets in separate Block-(Twitter:Independent embedded).The module also provides a field which can be added to specefic content and get different embedded tweets on different pages having different search query field.'),
    ];
    $form['result_type'] = [
      '#type' => 'radios',
      '#default_value' => 'recent',
      '#options' => [
        'mixed' => $this->t('Mixed'),
        'recent' => $this->t('Recent'),
        'popular' => $this->t('Popular'),
      ],
      '#title' => $this->t('Result Type'),
      '#required' => TRUE,
      '#description' => $this->t('Gives results you would prefer to receive. Valid values include: * Mixed: Include both popular and real time results in the response. * recent: return only the most recent results in the response * popular: return only the most popular results in the response.<a href="https://dev.twitter.com/rest/reference/get/search/tweets">Read More</a>'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * Saving Twitter Block Configuration.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->configFactory->get('twitter_search_block.settings');
    $values = $form_state->getValues();
    $config->set('tweets_count', $values['tweets_count'])
      ->set('search_query_value', $values['search_query_value'])
      ->set('result_type', $values['result_type'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
