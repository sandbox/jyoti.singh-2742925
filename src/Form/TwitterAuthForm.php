<?php

namespace Drupal\twitter_search_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides OAuth configuration options.
 */
class TwitterAuthForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'twitter_oAuth_form_id';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['twitter_search_block.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('twitter_search_block.settings');
    $form['oauth_access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth Access Token'),
      '#required' => TRUE,
      '#default_value' => $config->get('oauth_access_token'),
      '#placeholder' => $this->t('OAUTH ACCESS TOKEN *'),
    ];
    $form['oauth_access_token_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth Access Token Secret'),
      '#required' => TRUE,
      '#default_value' => $config->get('oauth_access_token_secret'),
      '#placeholder' => $this->t('OAUTH ACCESS TOKEN SECRET *'),
    ];
    $form['consumer_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consumer Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('consumer_key'),
      '#placeholder' => $this->t('CONSUMER KEY *'),
    ];
    $form['consumer_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consumer Secret'),
      '#required' => TRUE,
      '#default_value' => $config->get('consumer_secret'),
      '#placeholder' => $this->t('CONSUMER SECRET *'),
    ];
    $form['api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter API Url.'),
      '#required' => TRUE,
      '#placeholder' => $this->t('TWITTER API VERSION *'),
      '#default_value' => $config->get('api_version'),
      '#description' => $this->t('Latest Url for the Twitter Rest Request.'),
    ];
    $form['oembed_api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter Oembed API Url.'),
      '#required' => TRUE,
      '#placeholder' => $this->t('TWITTER OEMBED API VERSION *'),
      '#default_value' => $config->get('oembed_api_version'),
      '#description' => $this->t('Latest Url for the Twitter Oembed Restful API.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * Saving OAuth credentials in configuration.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('twitter_search_block.settings');
    $values = $form_state->getValues();
    $config->set('oauth_access_token', $values['oauth_access_token'])
      ->set('oauth_access_token_secret', $values['oauth_access_token_secret'])
      ->set('consumer_key', $values['consumer_key'])
      ->set('consumer_secret', $values['consumer_secret'])
      ->set('api_version', $values['api_version'])
      ->set('oembed_api_version', $values['oembed_api_version'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
