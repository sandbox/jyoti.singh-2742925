<?php

namespace Drupal\twitter_search_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Url;

require_once 'TwitterAPIExchange.php';

/**
 * Provides a block for displaying twitter embedded timeline for node.
 *
 * @Block(
 * id = "tweet_block",
 * admin_label = @Translation("Twitter Block:Node"),
 * category = @Translation("Blocks")
 * )
 */
class TweetBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * Entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;
  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Implements \Drupal\block\BlockBase::build().
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, EntityStorageInterface $node_storage, EntityManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->entityManager = $entity_manager;
    $this->nodeStorage = $node_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity.manager')->getStorage('node'),
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $currentUrl = Url::fromRoute('<current>');
    $currentUrl = $currentUrl->getInternalPath();
    $url = explode('/', $currentUrl);
    $config = $this->configFactory->get('twitter_search_block.settings');
    $count = $config->get('tweets_count');
    $result_type = $config->get('result_type');
    $node = $this->entityManager->getStorage('node')->load($url[1]);
    if ($node) {
      $bundle_fields = $this->entityManager->getFieldDefinitions('node', $node->getType());
      foreach (array_keys($bundle_fields) as $field_name) {
        if ($bundle_fields[$field_name]->getType() == 'twitter_search_fieldtype') {
          $field_data[] = $field_name;
          break;
        }
      }
      $settings = [
        'oauth_access_token' => $config->get('oauth_access_token'),
        'oauth_access_token_secret' => $config->get('oauth_access_token_secret'),
        'consumer_key' => $config->get('consumer_key'),
        'consumer_secret' => $config->get('consumer_secret'),
      ];

      if (!empty($node->$field_data[0]->tweet_values)) {
        $search_value = $node->$field_data[0]->tweet_values;
        $result = TwitterAPIClass::twitterApi($search_value, $settings, $count, $result_type);
        return [
          '#children' => $result,
          '#cache' => [
            'max-age' => 0,
          ],
        ];
      }
    }
  }

}
