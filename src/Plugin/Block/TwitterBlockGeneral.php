<?php

namespace Drupal\twitter_search_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Config\ConfigFactory;

require_once 'TwitterAPIExchange.php';

/**
 * Provides block with embedded tweets for independent use.
 *
 * @Block(
 * id = "general_twitter_block",
 * admin_label = @Translation("Twitter Block:General"),
 * category = @Translation("Blocks")
 * )
 */
class TwitterBlockGeneral extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Implements \Drupal\block\BlockBase::build().
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('twitter_search_block.settings');
    $count = $config->get('tweets_count');
    $result_type = $config->get('result_type');
    $search_value = $config->get('search_query_value');
    $settings = [
      'oauth_access_token' => $config->get('oauth_access_token'),
      'oauth_access_token_secret' => $config->get('oauth_access_token_secret'),
      'consumer_key' => $config->get('consumer_key'),
      'consumer_secret' => $config->get('consumer_secret'),
    ];
    $object = new TwitterAPIClass();
    $result = $object->twitterApi($search_value, $settings, $count, $result_type);
    return [
      '#children' => $result,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
