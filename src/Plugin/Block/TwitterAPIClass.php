<?php

namespace Drupal\twitter_search_block\Plugin\Block;

/**
 * Twitter 1.1 Search and timeline API implementation.
 */

use Drupal\Component\Utility\UrlHelper;

/**
 * TwitterAPIClass to make Twitter API Request.
 */
class TwitterAPIClass {

  /**
   * Returns the embedded timeline of tweets.
   *
   * @param string $search_value
   *   Search value for embedded timeline.
   * @param array $settings
   *   The setting array of Access keys.
   * @param int $count
   *   The number of tweets
   *   Count for the number of tweets.
   * @param string $result_type
   *   Result type for the search: mixed,recent,polpular.
   */
  public function twitterApi($search_value, array $settings, $count, $result_type) {

    $output = '';
    $config = \Drupal::config('twitter_search_block.settings');
    // URL for REST request, see: https://dev.twitter.com/docs/api/1.1/
    $url = $config->get('api_version');

    $requestQuery = [
      'q' => $search_value ,
      'count' => $count,
      'result_type' => $result_type,
    ];

    $requestMethod = 'GET';
    $getfield = '?' . UrlHelper::buildQuery($requestQuery);
    $twitter = new TwitterAPIExchange($settings);
    $api_response = $twitter->setGetfield($getfield)
      ->buildOauth($url, $requestMethod)
      ->performRequest();

    $response = json_decode($api_response);

    foreach ($response->statuses as $response_array) {
      $tweet_id = $response_array->id_str;

      $request_oembed = [
        'id' => $tweet_id ,
        'hide_media' => 'true',
        'maxwidth' => 260,
        'hide_thread' => 'true',
      ];
      $url_oembed = $config->get('oembed_api_version');
      $requestMethod = 'GET';
      $getfield = '?' . UrlHelper::buildQuery($request_oembed);
      $twitter_object = new TwitterAPIExchange($settings);
      $api_timeline = $twitter_object->setGetfield($getfield)
        ->buildOauth($url_oembed, $requestMethod)
        ->performRequest();

      $response_timeline = json_decode($api_timeline);
      $output .= $response_timeline->html;
    }
    return $output;
  }

}
